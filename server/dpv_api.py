from flask import Flask, request
from flask_restful import Resource, Api
from flask_jsonpify import jsonify

from source import dpv
app = Flask(__name__)

#@app.route('/')
api = Api(app)

#all data from https://www.discounter-preisvergleich.de/
dpv = dpv.discounter_preisvergleich()



#show @ URL without a function
class start_display(Resource):
    def get(self):
        #introduction of possible functions
        introduction = open("introduction.txt","r")
        text = introduction.readlines()
        #clear text from \n
        clear_text = []
        for line in text:
            #line.rstrip('\r\n')
            clear_text.append(line.rstrip())
        return clear_text

#list of categories to a request
class categories(Resource):
        def get(self, product_request):
                dpv.extract_headers_and_tables(product_request) 
                dpv.create_categoryList()
                categoryList = dpv.get_categoryList()
                return jsonify(categoryList)

#list of products
class products(Resource):
        def get(self,product_category,number_of_results,product_request):
            dpv.extract_headers_and_tables(product_request)
            dpv.create_categoryList()
            lv_returnCode = dpv.create_productList(product_request,number_of_results,category=product_category)
            if lv_returnCode == 1:
                return "invalid category"
            return dpv.get_productList()
api.add_resource(categories, '/categories/<product_request>')
api.add_resource(products,"/products/<product_category>,<int:number_of_results>,<product_request>")
api.add_resource(start_display, '/')

if __name__=='__main__':
    app.run(host='0.0.0.0',port='5002')
