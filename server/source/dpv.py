import requests
import lxml.html as lh
import pandas as pd
import copy
import json
import difflib
import operator

#gx = global
#lx = local

#xa = array
#xo = object
#xv = variable

#example: ga = global Array

class discounter_preisvergleich:
    #html content
    ga_headerList = [] 
    ga_ContentAllTables = []
    
    #clean content
    ga_categoryList = []
    ga_productList = {} #json format

    #header definition of the results
    ga_columnHeader = ["unbekannt1","Name","Marke","Preis_ct","Mass","InfoTag","Discounter","unbekannt8"]
    #attributes of results
    ga_JsonStructure = ["Marke","Preis_ct","Mass","Discounter"] #name is the first key of the JSON file
    
    #product_request<->the Search Request @ https://www.discounter-preisvergleich.de/
    #return = a list of all categories the Products are seperated
    def extract_headers_and_tables(self,product_request):
        self.ga_headerList = []
        self.ContentAllTables = []
        self.ga_categoryList = []
        self.ga_productList = {}

        #create a Request of the website
        #&d = global --> all shops &d=netto
        lv_url = "https://www.discounter-preisvergleich.de/suche.php?s=" + str(product_request) + "&d=global"
        lo_page = requests.get(lv_url)
        #Store the contents of the website under doc
        lo_doc = lh.fromstring(lo_page.content)
        
        self.ga_headerList = lo_doc.xpath('//h2') #devide html in headers
        self.ga_ContentAllTables = lo_doc.xpath('//table') #devide in product tables

    #fill the array ga_categoryList with the Table titles
    def create_categoryList(self):
        for header in self.ga_headerList:
            lv_category = header.text_content()
            self.ga_categoryList.append(str(lv_category))
    
    #fills the array ga_productList
    #and set a Limit to number_of_results
    def create_productList(self,product_request,number_of_results,category):
        lv_returnCode = self._get_productsFromTable(category)
        #returnCode 1 no categoryMatch
        if lv_returnCode == 1:
            return 1

        #limit the Product list to number_of_results
        la_StringSimilarity = [] #every Product get a similarityValue to the Product Request
        #--> the products are suitable to the Request
        for product in self.ga_productList:
            la_SimilarityStructure = []
            lv_SimilarityValue = difflib.SequenceMatcher(a=product.lower(), b=product_request.lower()).ratio()
            la_SimilarityStructure.append(lv_SimilarityValue)
            la_SimilarityStructure.append(product)
            la_StringSimilarity.append(la_SimilarityStructure)
        la_StringSimilarity = sorted(la_StringSimilarity,key=operator.itemgetter(0), reverse=True)
        #delete the products with the worst Similarity
        #only if the Resultlist has more results than the wanted number_of_results
        i = 0
        for simInfo in la_StringSimilarity:
            i += 1
            if i > number_of_results:
                #structure simInfo = [simValue,Product name]
                del(self.ga_productList[simInfo[1]])


    def _get_productsFromTable(self,categoryRequest):

        if categoryRequest != "all":
            #searching only in one category
            lv_catIndex = 0
            for catName in self.ga_categoryList:
                lv_catIndex += 1
                if catName.lower() == str(categoryRequest).lower():
                     #create a deepcopy to use the table content a extra html page --> xpath possible
                    lo_tableDoc = copy.deepcopy(self.ga_ContentAllTables[lv_catIndex])
                    #get table elements
                    la_tableElements = lo_tableDoc.xpath('//tr')
                    break #the right table index to the Category was found
            try:
                #test if lo_tableDoc exists
                test = len(lo_tableDoc)
            except:
                return 1
        else:
            #searching in all categories
            la_tableElements = []
            for i in range(1,len(self.ga_ContentAllTables)):
                lo_tableDoc = copy.deepcopy(self.ga_ContentAllTables[i])
                la_tableElements = la_tableElements + lo_tableDoc.xpath('//tr')


        #Create empty list
        la_structure=[]
        for column_title in self.ga_columnHeader:
            la_structure.append((column_title,[]))

        for j in range(1,len(la_tableElements)):

            T=la_tableElements[j]

            #If row is not of size 8 the //tr data is not from the table with products
            if len(T)!=8:
               continue #jump in the next iteration

            #i is the index of our column
            i=0

            #Iterate through each element of the row
            for t in T.iterchildren():
                lv_data=t.text_content()
                #delete \n and \r
                lv_data_clear = lv_data.replace("\n","")
                lv_data_clear = lv_data_clear.replace("\r","")
                lv_data_clear = lv_data_clear.replace("!","")
                lv_data_clear = lv_data_clear.replace(".","")
                #clear spaces
                lv_data_clear = lv_data_clear.split(" ")
                lv_data_clear_without_spaces = ""
                #create values with spaces at the rigth place
                for word in lv_data_clear:
                    if word != "":
                        lv_data_clear_without_spaces += word
                        lv_data_clear_without_spaces += " "
                #Append the data to the empty list of the i'th column
                la_structure[i][1].append(lv_data_clear_without_spaces)
                #Increment i for the next column
                i+=1

        la_Dict = {}
        la_Dict={title:column for (title,column) in la_structure}
        self._create_to_JSON_Structure(la_Dict)

    #create a JSON from the ordered structure:
    #attribut: array 0-n with value
    #to
    #array 0-n with attributes = value
    def _create_to_JSON_Structure(self,orderedDataSet):
        print(orderedDataSet)
        self.ga_productList = {}
        #length of "name" is the same length like the other arrays in the Dataset
        #the last element is not a product
        for i in range(0,len(orderedDataSet["Name"])):
            la_productInformation = {}
            for attr in self.ga_JsonStructure:
                try:
                    la_productInformation[attr] = orderedDataSet[attr][i]
                except:
                    pass
            self.ga_productList[orderedDataSet["Name"][i]] = la_productInformation

    def get_categoryList(self):
        if self.ga_categoryList == []:
            return "no Match"
        else:
            return self.ga_categoryList

    def get_productList(self):
        if self.ga_productList == {}:
            return "no Match"
        else:
            return self.ga_productList
