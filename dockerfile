FROM python:3.6
RUN pip install Flask Flask-restful flask-jsonpify pandas 
RUN pip install requests lxml
RUN useradd -ms /bin/bash admin
USER admin
WORKDIR /server
COPY server /server
CMD ["python", "dpv_api.py"]
