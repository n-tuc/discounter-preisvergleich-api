# Discounter Preisvergleich API

Diese API dient zum Extrahieren von der Seite https://www.discounter-preisvergleich.de/.
Dabei wird die Ergebnistabelle verwendet, die man nach einer Suchanfrage über das Textfeld erhält.
Man erhält Produktnamen sowie: den Preis, die Marke, eine Maßangabe und den Discounter welcher das Produkt verkauft.

Für die Ergebnisse ist die Seite https://www.discounter-preisvergleich.de/ verantwortlich. Die Seite, sowie die API, garantiert keine Vollständigkeit und Korrektheit der Inhalte.

*Zugriff zur Zeit möglich:*
```
http://webengineering.ins.hs-anhalt.de:32232/<request>
```

## Verwendung

**Request "/categories"**
```
http://<server-url>/categories/<Produkt>
```
<Produkt> entspricht der Suchanfrage im Textfeld von Discounter-Preisvergleich

Mit diesem Request erhält man eine Liste von Kategorien, zu denen das Produkt zugeordnet werden kann. 
Wenn der Anfrage kein Produkt von Discounter-Preisvergleich zugeordnet werden kann, erhält man das Ergebnis "no Match".

**Request "/products"**
```
http://<server-url>/products/<category>,<Ergebnisanzahl>,<Produkt>
```
* <Produkt> entspricht der Suchanfrage im Textfeld von Discounter-Preisvergleich
* <Ergebnisanzahl> muss als Zahl angegeben werden! Schrängt die Ergebnisliste auf die Anzahl ein
* <category> ermöglicht die Suche in einer bestimmten Kategorie. Angabe "all" liefert Produkte aus allen Kategorien

Mit diesem Request erhält man eine List von Produkten mit den Informationen:
* Name
* Marke
* Preis in ct
* Maßinformationen z.B. Gewicht in g oder Volumen in ml (abhängig vom Produkt)
* Discounternamen, bei dem das Produkt verkauft wird

## Beispiel Requests
```
http://<server-url>/categories/Möhre
```
liefert alle Kategorien in denen Produkte passend zu "Möhre" auftauchen.
Ergebnis:
[
"Fertig­produkte",
"Saft",
"Gemüse",
"Salate"
]

```
http://<server-url>/products/all,20,Kartoffel
```
liefert alle Produkte, die mit dem Wort "Kartoffel" zu tun haben, unabhängig der Kategorie. Es liefert die 20 Produkte, die am "string ähnlichsten" in ihrem Namen zur Kartoffel sind.

```
http://<server-url>/products/Saft,50,Möhren
```
liefert alle Säfte denen der Begriff Möhre zugeordnet ist.
## Benötigte Python Module



## Deployment auf eigenen Server
Die Api kann man am einfachsten über ein Docker Container starten. 
Damit umgeht man die Installation der benötigten Python Module.
Einfach ein Docker Image erstellen und starten.
Gegebenenfalls den Port ändern, da der Webserver standardmäßig auf Port 5002 läuft.

Alternativ kann auch mit Python3 die Datei:
```
server/dpv_api.py
```
gestartet werden.


## Entwickler

* **Ernst Stötzner** 
ernst.stoetzner@student.hs-anhalt.de

## Verwendung in anderen Projekt
FiMyPro - Find my product!
https://gitlab.com/n-tuc/fimypro